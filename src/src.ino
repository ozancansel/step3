#include "step3.h"

void setup(){
    //Do not change order
    serial_init();
    file_init();
    protocol_init();
    program_init();
    
    report_init();
}

void loop(){
    protocol_loop();
    program_loop();
}
