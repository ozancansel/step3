#ifndef SHARED_BUFFER_H
#define SHARED_BUFFER_H

void add_to_buffer(uint8_t c);
void add_str_to_buffer(const char* str);
void reset_buffer();
char* get_buffer();
uint8_t get_buffer_length();
uint8_t read_byte(uint8_t idx);
uint8_t buffer_occupied();

#endif
