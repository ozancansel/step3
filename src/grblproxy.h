#ifndef GRBL_PROXY_H
#define GRBL_PROXY_H

#define GRBL_UNKNOWN  0
#define GRBL_RUN      1
#define GRBL_JOG      2
#define GRBL_ALARM    3
#define GRBL_IDLE     4

#define MESSAGE_GRBL_INITIALIZED_BIT   (0)
#define MESSAGE_HOMING_OR_UNLOCK_BIT   (1)
#define MESSAGE_GRBL_INITIALIZED_MASK  (1 << MESSAGE_GRBL_INITIALIZED_BIT)
#define MESSAGE_HOMING_OR_UNLOCK_MASK  (1 << MESSAGE_HOMING_OR_UNLOCK_BIT)

uint8_t  wait_for_ok();

uint8_t wait_for_motion_finish();

uint8_t reset_grbl_sync();

uint8_t grbl_unlock();

#endif
