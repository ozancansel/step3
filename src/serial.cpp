#include "step3.h"

SoftwareSerial grblSerial(9 , 10);

Stream* grblStream;
Stream* hostStream;

void serial_init(){

    Serial.begin(9600);
    grblSerial.begin(9600);

    grblStream = &grblSerial;
    hostStream = &Serial;
}

uint8_t host_has_data(){
  return hostStream->available();
}

uint8_t grbl_has_data(){
  return grblStream->available();
}

void print_to_grbl(char* line){
    grblStream->print(line);
}

void print_to_host(char* str){
    hostStream->print(str);
}

void print_to_host(const __FlashStringHelper * str){
  hostStream->print(str);
}

void print_to_host(const string_ref_t &ref){
  for(uint8_t i = ref.start; i < ref.end; i++){
      write_to_host(ref.buffer[i]);
  }
}

void print_to_host(int16_t val){
  hostStream->print(val);
}

int read_from_host(){
    if(hostStream->available()){
        return hostStream->read();
    }
    else {
        return 0;
    }
}

uint8_t read_from_grbl(){
    if(grblStream->available())
        return grblStream->read();
    else
        return 0;
}

void write_to_grbl(uint8_t c){
    grblStream->write(c);
}

void write_to_host(uint8_t c){
    hostStream->write(c);
}

void host_flush(){
    hostStream->flush();
}

void clear_grbl_buffer(){
    //Clear buffer
    while(grbl_has_data())
      read_from_grbl();
}

