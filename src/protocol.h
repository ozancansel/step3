#ifndef PROTOCOL_H
#define PROTOCOL_H

#define PROTOCOL_PROGRAM_RUNNING    (1 << 0)    //Program running bit

#define PROTOCOL_ENGINE_CMD_START   95 //'_'
#define PROTOCOL_CMD_SEPERATOR      44 //','
#define PROTOCOL_CMD_END            64 //'@'
 
#define PROTOCOL_AWAIT          0
#define PROTOCOL_GRBL_LISTEN    1
#define PROTOCOL_SD_LISTEN      2
#define PROTOCOL_HOST_LISTEN    3

#define PROTOCOL_GRBL_REPORT_ST     '?'
#define PROTOCOL_GRBL_RESET         18  //Ctrl+X

//Command
#define CMD_APPEND_PROG       97  //a
#define CMD_TRUNCATE_PROG     98  //b
#define CMD_RUN_PROG          99  //c
#define CMD_STOP_PROG         100 //d
#define CMD_READ_PROG         101 //e

void protocol_init();

void protocol_loop();

uint8_t is_grbl_code();

uint8_t read_code();

template<typename T>
T read_next_number();

string_ref_t read_next_str();

uint8_t read_next_byte();

void    skip_whitespace();

void    reset_parse_idx();

void    host_cmd_callback();

#endif
