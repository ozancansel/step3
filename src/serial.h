#ifndef SERIAL_H
#define SERIAL_H

extern Stream* grblStream;
extern Stream* hostStream;

//Common
void serial_init();

//Host

uint8_t host_has_data();

void print_to_host(char* str);

void print_to_host(const __FlashStringHelper* str);

void print_to_host(const string_ref_t &ref);

void print_to_host(int16_t val);

void write_to_host(uint8_t c);

int read_from_host();

void host_flush();

//Grbl

uint8_t grbl_has_data();

void print_to_grbl(char* line);

void write_to_grbl(uint8_t c);

uint8_t read_from_grbl();

void clear_grbl_buffer();

#endif
