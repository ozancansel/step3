#ifndef CONFIG_H
#define CONFIG_H

#define BUFFER_SIZE                 100
#define PROTOCOL_CMD_TERMINATOR     64  //'@'
#define FUNC_ERROR_RETURN           0
#define FUNC_SUCCESS_RETURN         1

#endif
