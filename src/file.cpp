#include "step3.h"

SdFat sd;
SdFile file;
char buf[4];
uint8_t file_state;
uint8_t f_id;

//Inits file sytem
void file_init(){
  if(!sd.begin(4 , SD_SCK_MHZ(50))){
    print_to_host(F("Sd card couldn't initialized.\n"));
  }

  file_state = 0;
  f_id = 0;
}

//Opens file for reading
uint8_t open_for_read(uint8_t id){
  //Convert int id to string
  itoa(id , buf , 4);
  if(!file.open(buf , FILE_READ_ONLY_BIT_MASK)){
    return FUNC_ERROR_RETURN;
  }

  //Assign id
  f_id = id;
  //Set new state
  file_state = FILE_READ_ONLY_BIT_MASK;
  return FUNC_SUCCESS_RETURN;
}

//Opens file for writing
uint8_t open_for_write(uint8_t id){
  itoa(id , buf , 4);
  if(!file.open(buf , FILE_WRITE_ONLY_BIT_MASK)){
    return FUNC_ERROR_RETURN;
  }

  //Assign id
  f_id = id;
  //Set new state
  file_state = FILE_WRITE_ONLY_BIT_MASK;
  return FUNC_SUCCESS_RETURN;
}

uint8_t openf(uint8_t id , uint8_t opts){
  itoa(id , buf , 4);

  if(!file.open(buf , opts)){
    return FUNC_ERROR_RETURN;
  }

  //Assign id
  f_id = id;
  //Set new state
  file_state = opts;
  return FUNC_SUCCESS_RETURN;
}

//Prints to file
uint8_t print_to_file(string_ref_t &strRef){
  StringRef ref(strRef);

  return print_to_file(ref);
}

uint8_t print_to_file(StringRef &ref){
    uint16_t len = ref.len();
  for(uint16_t i = 0; i < len; i++){
    if(!file.write(ref[i])){
      return FUNC_ERROR_RETURN;
    }
  }

  return FUNC_SUCCESS_RETURN;
}

uint8_t print_to_file(char* buf){
  uint16_t len = strlen(buf);
  uint16_t written = 0;

  //If buf is empty, return without writing
  if(len == 0)
    return FUNC_SUCCESS_RETURN;
  
  for(uint16_t i = 0; i < len; i++){
    if(!file.write(buf[i])){
      return FUNC_ERROR_RETURN;
    }

    written++;
  }

  return written;
}

//Appends the byte to the file
uint8_t write_to_file(uint8_t c){
  return file.write(c);
}

//Truncates the file
uint8_t file_truncate(uint8_t id){
  itoa(id , buf , 4);
  
  if(!file.open(buf , FILE_TRUNCATE_MASK)){
    return FUNC_ERROR_RETURN;
  }

  //Close file
  close();
  
  return FUNC_SUCCESS_RETURN;
}

//Read line from file and allocates to shared buffer
uint8_t read_line(){
  return 1;
}

//Returns 1 if at the end of the file
uint8_t  has_file_data(){
  return file.available() > 0;
}

//Reads a byte from file
uint8_t file_read_byte(){
  return file.read();
}

//Returns file size
uint32_t  file_size(){
  return file.fileSize();
}

//Closes the file which is opened
void close(){
  //Clear id
  f_id = 0;
  //Closes file
  file.close();
}

//Checks file cursor position and returns 1 if the cursor is at end of the file, otherwise 0
uint8_t is_at_eof(){
  return file.available() == 0;
}

//Seeks the specified position
//File should be opened
void file_seek(uint32_t pos){
  return file.seekSet(pos);
}

//Returns 1 if the file is empty, otherwise 0
//File should be opened and in read only mode
uint8_t file_is_empty(){
  return file.fileSize() == 0;
}

uint8_t file_id(){
  return f_id;
}

