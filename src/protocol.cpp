#include "step3.h"

uint8_t protocol_state;
uint8_t parse_idx;

void protocol_init()
{
    //State reset
    protocol_state = PROTOCOL_AWAIT;
    reset_parse_idx();
}

void protocol_loop(){
    uint8_t c;

    //Clearing buffer
    while(host_has_data()){
        //If protocol is at idle
        if (protocol_state == PROTOCOL_AWAIT)
        {

            //Reset buffer and parsing index
            reset_parse_idx();
            reset_buffer();
            
            //Read from host
            if (c = read_from_host())
            {
        
                //If grbl command is receiving
                if (c != PROTOCOL_ENGINE_CMD_START)
                {
                    //Yes it is ensured that the command is not related with industrial engine
                    //but this block is added to ensure that the command type is grbl
                    //It provides more reliable communication. This block prevents us from unstable response in such situations which are sending newline or carriage return after engine command or just newline command
                    if(
                       c == 'G' ||  //GCode
                       c == 'M' ||  //MCode
                       c == '$' ||  //Settings cmd or jog start
                       c == 0x18 || //Soft Reset
                       c == '?'  || //Statys query report
                       c == '~'  || //Cycle Start Resume
                       c == '!'  || //Feed hold
                       c == 0x84 || //Safety Door
                       c == 0x85 || //Jog Cancel
                       c == 0x90 || //Feed Override Set %100 of programmed rate
                       c == 0x91 || //Feed Override Increase %10
                       c == 0x92 || //Feed Override Decrease %10
                       c == 0x93 || //Feed Override Increase %1
                       c == 0x94 || //Feed Override Decrease %1
                       c == 0x95 || //Rapid Override Set %100
                       c == 0x96 || //Rapid Override Set %50
                       c == 0x97 || //Rapid Override Set %25
                       c == 0x99 || //Spindle Override Set %100
                       c == 0x9A || //Spindle Override Increase %10
                       c == 0x9B || //Spinde Override Decrease %10
                       c == 0x9C || //Spindle Override Increase %1
                       c == 0x9D || //Spindle Override Decrease %1
                       c == 0x9E || //Toggle spindle stop
                       c == 0xA0 || //Togle Flood Coolant
                       c == 0xA1 //Toggle Mist coolant
                       ){
                          
                          //Change state
                        protocol_state = PROTOCOL_GRBL_LISTEN;
                        //Redirecting to grbl
                        write_to_grbl(c);
        
                        //If grbl command is special command which is just one byte, then revert back to AWAIT state
                        if(  c == PROTOCOL_GRBL_RESET ||
                             c == PROTOCOL_GRBL_REPORT_ST ||
                             c == 0x18 ||
                             c == '?' || //Status query report
                             c == 0x84 || //Safety Door
                             c == 0x85 || //Jog Cancel
                             c == 0x90 || //Feed Override Set %100 of programmed rate
                             c == 0x91 || //Feed Override Increase %10
                             c == 0x92 || //Feed Override Decrease %10
                             c == 0x93 || //Feed Override Increase %1
                             c == 0x94 || //Feed Override Decrease %1
                             c == 0x95 || //Rapid Override Set %100
                             c == 0x96 || //Rapid Override Set %50
                             c == 0x97 || //Rapid Override Set %25
                             c == 0x99 || //Spindle Override Set %100
                             c == 0x9A || //Spindle Override Increase %10
                             c == 0x9B || //Spinde Override Decrease %10
                             c == 0x9C || //Spindle Override Increase %1
                             c == 0x9D || //Spindle Override Decrease %1
                             c == 0x9E || //Toggle spindle stop
                             c == 0xA0 || //Togle Flood Coolant
                             c == 0xA1 //Toggle Mist coolant
                       ){
                            protocol_state = PROTOCOL_AWAIT;
                       }
                    }
                }
                else
                {
                    //Add char to buffer
                    add_to_buffer(c);
                    //Changing state
                    protocol_state = PROTOCOL_HOST_LISTEN;
                }
            }
        }
        //Listen host commands
        else if (protocol_state == PROTOCOL_HOST_LISTEN)
        { //If host sending command
            //Adding new char to buffer
            if (c = read_from_host())
            {
                //Host command finished
                //If terminator received and previous byte is not escaping character, then command finished
                if (c == PROTOCOL_CMD_TERMINATOR)
                {
                    add_to_buffer('\0');
                    //Trigger command callback
                    host_cmd_callback();
                    //Reset buffer and idx
                    reset_parse_idx();
                    reset_buffer();
                    //Reset to idle state
                    protocol_state = PROTOCOL_AWAIT;
                }
                else {
                    add_to_buffer(c);
                }
            }
        }
        //Grbl commands redirecting directly
        else if (protocol_state == PROTOCOL_GRBL_LISTEN){
            if(c = read_from_host()){
    
                write_to_grbl(c);
    
                //If data available and terminator char is received
                if(c == '\n'){
                    protocol_state = PROTOCOL_AWAIT;
                }
            }
        }
    }

    //If grbl has data
    while(grbl_has_data()){
      write_to_host(read_from_grbl());
    }
}

template<typename T>
T read_next_number(){
  string_ref_t strRef = read_next_str();

  T val = 0;
  T multiplier = 1;

  uint8_t start = strRef.start;
  int8_t sign = 1;
  
  //Check sign
  if(read_byte(start) == 45){
    sign = -1;
    start++;
  }

  //Create number from string
  for(uint8_t i = strRef.end - 1; i >= start; i--){
    uint8_t b = read_byte(i);
    b -= 48;
    val += multiplier * b;
    multiplier *= 10;
  }

  val *= sign;

  return val;
}

void host_cmd_callback(){
  if(is_grbl_code()){
    //Stream buffer to grbl
    print_to_grbl(get_buffer());
    reset_buffer();
    return;
  }

  //Code reading
  uint8_t code = read_code();

  switch(code){
    case CMD_APPEND_PROG : {
      uint8_t prog_id = read_next_number<uint8_t>();
      //Open file for write
      if(!open_for_write(prog_id)){
        report_err(CMD_APPEND_PROG);
        return;
      }
      
      string_ref_t str_ref;
      str_ref.start = parse_idx;
      str_ref.end = get_buffer_length() - 1;
      str_ref.buffer = get_buffer();
      int8_t res = print_to_file(str_ref);
      
      //Closing file
      close();
      
      //Reporting result
      report_cmd_append(res);
    }
    break;
    case CMD_TRUNCATE_PROG : {
      //Truncating main program
      uint8_t file_id = read_next_number<uint8_t>();
      uint8_t attemp_count = 0;

      file_truncate(file_id);

      open_for_read(FILE_MAIN_PROG);

      if(!file_is_empty()){
        //Close file
        close();
        file_truncate(file_id);
        print_to_host("Truncating\n");
      } else {
        close();
        break;
      }


      report_success(CMD_TRUNCATE_PROG);
    }
    break;
    case CMD_RUN_PROG :{
      //If program is running
      if(p_is_running()){
        report_err(CMD_RUN_PROG);
        break;
      }
      
      uint8_t opts = read_next_number<uint8_t>();
      //Starting program
      program_start(opts);
    }
    break;
    case CMD_STOP_PROG :
      if(!p_is_running()){
        report_success(CMD_STOP_PROG);
        break;
      }

      //Stops the program
      program_stop();
      delay(2000);
      report_success(CMD_STOP_PROG);
      
    break;
    case CMD_READ_PROG :  {
      if(p_is_running()){
        report_err(CMD_READ_PROG);
        break;
      }
      uint8_t prog_id = read_next_number<uint8_t>();
      
      //open for read
      if(!open_for_read(prog_id)){
        //If file could not open, report error
        report_err(CMD_READ_PROG);
        close();
        return;
      }

      write_to_host(PROTOCOL_ENGINE_CMD_START);
      write_to_host(CMD_READ_PROG);
      write_to_host(PROTOCOL_CMD_SEPERATOR);
      print_to_host(prog_id);
      write_to_host(PROTOCOL_CMD_SEPERATOR);

      uint8_t idx = 0;
      
      //Read file and write to host
      while(has_file_data()){
        write_to_host(file_read_byte());
        if(idx > 60){
          host_flush();
          idx = 0;
        } else {
          idx++;
        }
      }

      //Close file
      close();

      //End of content
      write_to_host(PROTOCOL_CMD_TERMINATOR);

      //Flush host stream
      host_flush();
    }
    break;
  }

  reset_buffer();
  reset_parse_idx();
}

uint8_t is_grbl_code(){
  return get_buffer()[0] != PROTOCOL_ENGINE_CMD_START;
}

uint8_t read_code(){
  return get_buffer()[1];
}

string_ref_t read_next_str(){
  //skipping whitespaces
  skip_whitespace();

  //Create ref object
  string_ref_t  ref;
  ref.buffer = get_buffer();
  ref.start = parse_idx;
  
  //If parse overflow
  if(parse_idx > get_buffer_length()){
    ref.buffer = NULL;
    ref.start = 0;
    ref.end = 0;
    return ref;
  }

  uint8_t c = 15;
  
  while((c = read_next_byte()) && c != '\0' && c != PROTOCOL_CMD_SEPERATOR){
    //Waiting for end of command
  }
  
  ref.end = parse_idx - 1;
  
  return ref;
}

uint8_t read_next_byte(){
  uint8_t b = get_buffer()[parse_idx];
  parse_idx++;
  return b;
}

void skip_whitespace(){
  uint8_t c = 0;
  while((c = read_next_byte) && c != '\0' && c == PROTOCOL_CMD_SEPERATOR){   }
}

void reset_parse_idx(){
  parse_idx = 3;
}
