#include "step3.h"

uint8_t program_state;

void program_init(){
  program_state = 0;
}

void program_loop(){
  
  //If program is not running
  if((program_state & (PROGRAM_RUNNING_MASK)) == 0){
      return;
  }

  //Return due to buffer occupied
  if(buffer_occupied()){
    return;
  }

  //Program is running if reaches here
  uint8_t c;

  //If pre run script wasn't run
  //If pre run flag is 1
  if(program_state & PROGRAM_PRE_RUN_MASK){

    //Resetting grbl
    reset_grbl_sync();

    grbl_unlock();
    
    //If pre program file could not opened
    if(!open_for_read(FILE_PRE_PROG)){
      report_err(CMD_RUN_PROG);
      //Called to just ensure
      close();
      return;
    }

    //Stream file
    while(has_file_data()){
      stream_line();
    }
    
    close();
    
    //Set pre run flag 0
    program_state &= ~PROGRAM_PRE_RUN_MASK;
    //Take rest after pre-program until next call
    return;
  }
  
  //If main file couldn't opened
  if(file_id() != FILE_MAIN_PROG){
    //If another file is opened
    if(file_id() != 0){
      //Close file
      close();
    }
    //Open main file
    if(!open_for_read(FILE_MAIN_PROG)){
      return;
    }
  }
  
  //Stream one line
  stream_line();

  //Is at eof
  if(is_at_eof()){
    if(program_state & PROGRAM_LOOP_MODE_BIT){
      close();
    }
    else {
      //Close file
      close();
      //Wait motion to end
      wait_for_motion_finish();
    }
  }
}

void program_start(uint8_t options){
  program_state = 0;
  program_state |= PROGRAM_RUNNING_MASK | PROGRAM_PRE_RUN_MASK | PROGRAM_POST_RUN_MASK;
  program_state |= options;
}

void program_stop(){
  program_state = 0;
}

void hard_stop(){
  program_state = 0;
  
  //Resets grbl
  write_to_grbl(0x18);
}

uint8_t stream_line(){
  uint8_t c;
  uint8_t res = FUNC_ERROR_RETURN;
  
  while(has_file_data()){
    c = file_read_byte();
    add_to_buffer(c);
    if(c == '\n'){
      //Append terminator char
      add_to_buffer('\0');
      //Clear grbl buffer to prevent unexpected responses
      clear_grbl_buffer();
      host_cmd_callback();
      if(!wait_for_ok()){
      } else {
      }
      res = FUNC_SUCCESS_RETURN;
      return res;
    }
  }

  //Reset buffer
  reset_buffer();
  
  //If reaches here, it return error
  return res;
}

uint8_t p_is_running(){
  return program_state & PROGRAM_RUNNING_MASK;
}

