#include "step3.h"

uint8_t  wait_for_ok(){
  uint8_t idx = 0;
  uint8_t c;
  while(true){
    if(grbl_has_data()){
      //Reading grbl
      c = read_from_grbl();

      //If grbl command finished
      if(c == '\n'){
        //End buffer
        add_to_buffer('\0');
        print_to_host("Received : ");
        print_to_host(get_buffer());
        print_to_host("\n");
        if(str_contains(get_buffer() , PSTR("ok"))){
          reset_buffer();
          return FUNC_SUCCESS_RETURN;
        } else if(str_contains(get_buffer() , PSTR("error"))){
          reset_buffer();
          return FUNC_ERROR_RETURN;
        }
      } else {
        //Add char to buffer
        add_to_buffer(c);
        write_to_host(c);
      }
    }
  }
}

uint8_t wait_grbl_to_initialize(){
  uint8_t idx = 0;
  uint8_t c;
  uint8_t result = 0;
  while(!result){
    while(grbl_has_data()){
      //Reading grbl
      c = read_from_grbl();

      //If grbl command finished
      if(c == '\n' && idx > 1){
        //End buffer
        add_to_buffer('\n');
        add_to_buffer('\0');
        print_to_host(get_buffer());
        
        if(str_contains(get_buffer() , PSTR("Grbl 1."))){
          result |= MESSAGE_GRBL_INITIALIZED_MASK;
          reset_buffer();
          print_to_host("Grbl Initialized...\n");
          delay(50);
        }
        
        if(str_contains(get_buffer() , PSTR("[MSG:'$H'|'$X' to unlock]"))){
          result |= MESSAGE_HOMING_OR_UNLOCK_MASK;
          reset_buffer();
          print_to_host("Unlock or homing\n");
        }
      } else {
        //Add char to buffer
        add_to_buffer(c);
      }

      idx++;
    }
  }
  
  reset_buffer();

  return result;
}

uint8_t wait_for_motion_finish(){
  return 1;
}

uint8_t reset_grbl_sync(){
  clear_grbl_buffer();
  write_to_grbl(0x18);
  return wait_grbl_to_initialize();
}

uint8_t grbl_unlock(){
  clear_grbl_buffer();
  print_to_grbl(PSTR("$X\n"));
  wait_for_ok();
}

