#include "step3.h"

void report_cmd_append(int8_t res){
    //If res is less than zero, it means error occurred
    if(res){
        write_to_host(PROTOCOL_ENGINE_CMD_START);
        write_to_host(CMD_APPEND_PROG);
        write_to_host(PROTOCOL_CMD_SEPERATOR);
        write_to_host('1');
        write_to_host(PROTOCOL_CMD_SEPERATOR);
        print_to_host(res);
        write_to_host(PROTOCOL_CMD_TERMINATOR);
    } else {
        write_to_host(PROTOCOL_ENGINE_CMD_START);
        write_to_host(CMD_APPEND_PROG);
        write_to_host(PROTOCOL_CMD_SEPERATOR);
        write_to_host('0');
        write_to_host(PROTOCOL_CMD_TERMINATOR);
    }
}

void report_success(uint8_t code){
    write_to_host(PROTOCOL_ENGINE_CMD_START);
    write_to_host(code);
    write_to_host(PROTOCOL_CMD_SEPERATOR);
    write_to_host('1');
    write_to_host(PROTOCOL_CMD_TERMINATOR);
}

void report_err(uint8_t code){
    write_to_host(PROTOCOL_ENGINE_CMD_START);
    write_to_host(code);
    write_to_host(PROTOCOL_CMD_SEPERATOR);
    write_to_host('0');
    write_to_host(PROTOCOL_CMD_TERMINATOR);
}

void report_init(){
    print_to_host(F("Roboskop Step-3\n"));
}

