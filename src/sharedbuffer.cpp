#include "step3.h"

uint8_t shared_buffer[BUFFER_SIZE];
uint8_t buffer_idx = 0;

void add_to_buffer(uint8_t c){
    shared_buffer[buffer_idx] = c;
    buffer_idx++;    
}

void add_str_to_buffer(const char* str){
  uint8_t len = strlen_P(str);

  for(uint8_t i = 0; i < len; i++){
    add_to_buffer(pgm_read_byte(&str[i]));
  }
}

void reset_buffer(){
    buffer_idx = 0;
    shared_buffer[0] = '\0';
}

char* get_buffer(){
  return shared_buffer;
}

uint8_t get_buffer_length(){
  return buffer_idx;
}

uint8_t read_byte(uint8_t idx){
  return shared_buffer[idx];
}

uint8_t buffer_occupied(){
  return buffer_idx > 0;
}

