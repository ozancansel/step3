#ifndef FILE_H
#define FILE_H

#define ENABLE_FILE_WARNINGS

#define FILE_READ_ONLY_BIT_MASK  (O_CREAT  | O_READ)
#define FILE_WRITE_ONLY_BIT_MASK (O_CREAT | O_WRITE | O_APPEND)
#define FILE_TRUNCATE_MASK       (O_CREAT | O_TRUNC | O_WRITE)

//Inits file sytem
void file_init();

//Opens file for reading
uint8_t open_for_read(uint8_t id);

//Opens file for writing
uint8_t open_for_write(uint8_t id);

uint8_t openf(uint8_t id , uint8_t mask);

//Prints to file
uint8_t print_to_file(string_ref_t &strRef);

//Prints ref to file
uint8_t print_to_file(StringRef &ref);

uint8_t print_to_file(char* buf);

//Appends the byte to the file
uint8_t write_to_file(uint8_t c);

//Truncates the file
uint8_t file_truncate(uint8_t file);

//Read line from file and allocates to shared buffer
uint8_t read_line();

//Returns 1 if at the end of the file
uint8_t  has_file_data();

//Reads a byte from file
uint8_t file_read_byte();

//Closes the file which is opened
void close();

//Checks file cursor position and returns 1 if the cursor is at end of the file, otherwise 0
uint8_t is_at_eof();

//Seeks the specified position
//File should be opened
void file_seek(uint32_t pos);

//Returns 1 if the file is empty, otherwise 0
//File should be opened and in read only mode
uint8_t file_is_empty();

uint8_t file_id();

#endif
