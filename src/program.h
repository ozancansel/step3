#ifndef PROGRAM_H
#define PROGRAM_H

//Bit
#define PROGRAM_RUNNING_BIT     0
#define PROGRAM_LOOP_MODE_BIT   1
#define PROGRAM_PRE_RUN_BIT     2
#define PROGRAM_POST_RUN_BIT    3

//Mask
#define PROGRAM_RUNNING_MASK    (1 << PROGRAM_RUNNING_BIT)
#define PROGRAM_LOOP_MODE_MASK  (1 << PROGRAM_LOOP_MODE_BIT)
#define PROGRAM_PRE_RUN_MASK    (1 << PROGRAM_PRE_RUN_BIT)
#define PROGRAM_POST_RUN_MASK   (1 << PROGRAM_POST_RUN_BIT)

//Files
#define FILE_MAIN_PROG    1
#define FILE_PRE_PROG     2
#define FILE_POST_PROG    3

//Init programs
void program_init();

//Should be called from main loop
//It drives the program
void program_loop();

//Tell to program_loop to start with specified options
//Program actually will run on program_loop
void program_start(uint8_t options);

//Stop the program
void program_stop();

//Stops grbl hardly
void hard_stop();

//Reads line from the file and streams to engine command parser
uint8_t stream_line();

//Returns 1 if the program is running , otherwise 0
uint8_t p_is_running();

#endif
