#ifndef REPORT_H
#define REPORT_H

void report_cmd_append(int8_t res);
void report_success(uint8_t code);
void report_err(uint8_t code);
void report_init();

#endif
