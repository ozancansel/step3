#ifndef UTILS_H
#define UTILS_H

int8_t str_contains(char* src , const __FlashStringHelper* str);
int8_t str_contains(char* src , const char* str);

#endif
