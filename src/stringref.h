#ifndef STRING_REF_H
#define STRING_REF_H

typedef struct {
  char*   buffer;
  uint8_t start;
  uint8_t end;
} string_ref_t;

class StringRef{

  public:
    StringRef(string_ref_t ref);
    StringRef(int16_t start , int16_t end , char* str);
    uint16_t  len();
    inline uint8_t  operator[](int16_t idx){
      return m_str[idx + m_start];
    }
  private:
    int16_t   m_start;
    int16_t   m_end;
    char*     m_str;
};

#endif
