#include "step3.h"

int8_t str_contains(char* src , const __FlashStringHelper* str){
    return strstr_P(src , reinterpret_cast<PGM_P>(str));
}

int8_t str_contains(char* src , const char* str){
    return strstr_P(src , str);
}

