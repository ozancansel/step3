#include "step3.h"


StringRef::StringRef(string_ref_t ref)
:
m_start(ref.start) , 
m_end(ref.end) ,
m_str(ref.buffer)
{
  
}

StringRef::StringRef(int16_t start , int16_t end , char* str)
:
m_start(start) ,
m_end(end) ,
m_str(str)
{
  
}


uint16_t StringRef::len(){
  return m_end - m_start;
}

